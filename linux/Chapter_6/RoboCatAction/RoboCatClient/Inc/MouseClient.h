#ifndef MouseClient_h
#define MouseClient_h

class MouseClient : public Mouse
{
public:
	static	GameObjectPtr	StaticCreate()		{ return GameObjectPtr( new MouseClient() ); }

protected:
	MouseClient();

private:

	SpriteComponentPtr	mSpriteComponent;
};

#endif //MouseClient_h
