#include "RoboCatPCH.h"


int UDPSocket::Bind( const SocketAddress& inBindAddress )
{
	int result = bind( mSocket, &inBindAddress.mSockAddr, inBindAddress.GetSize() );
	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::Bind");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	
	return NO_ERROR;
}

int UDPSocket::SendTo( const void* inToSend, int inLength, const SocketAddress& inToAddress )
{
	int byteSentCount = sendto( mSocket,
#ifdef _WIN32
			static_cast<const char*>(inToSend),
#else
			inToSend,
#endif
			inLength, 0, &inToAddress.mSockAddr, inToAddress.GetSize());
	if( byteSentCount <= 0 )
	{
		string msg("UDPSocket::SendTo");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return byteSentCount;
	}
}

int UDPSocket::ReceiveFrom( void* inToReceive, int inMaxLength, SocketAddress& outFromAddress )
{
	socklen_t fromLength = outFromAddress.GetSize();
	
	int readByteCount = recvfrom( mSocket,
#ifdef _WIN32
			static_cast<char*>(inToReceive),
#else
			inToReceive,
#endif
			inMaxLength, 0, &outFromAddress.mSockAddr, &fromLength);
	if( readByteCount >= 0 )
	{
		return readByteCount;
	}
	else
	{
		int error = SocketUtil::GetLastError();
		
		if( error == WSAEWOULDBLOCK )
		{
			return 0;
		}
		else if( error == WSAECONNRESET )
		{
			//this can happen if a client closed and we haven't DC'd yet.
			//this is the ICMP message being sent back saying the port on that computer is closed
			LOG( "Connection reset from %s", outFromAddress.ToString().c_str() );
			return -WSAECONNRESET;
		}
		else
		{
			string msg("UDPSocket::ReceiveFrom");
#ifndef _WIN32
			msg += string(" -> ") + string(strerror(errno));
#endif
			SocketUtil::ReportError(msg.c_str());
			return -error;
		}
	}
}

UDPSocket::~UDPSocket()
{
#if _WIN32
	closesocket( mSocket );
#else
	close( mSocket );
#endif
}


int UDPSocket::SetNonBlockingMode( bool inShouldBeNonBlocking )
{
#if _WIN32
	u_long arg = inShouldBeNonBlocking ? 1 : 0;
	int result = ioctlsocket( mSocket, FIONBIO, &arg );
#else
	int flags = fcntl( mSocket, F_GETFL, 0 );
	flags = inShouldBeNonBlocking ? ( flags | O_NONBLOCK ) : ( flags & ~O_NONBLOCK);
	int result = fcntl( mSocket, F_SETFL, flags );
#endif
	
	if( result == SOCKET_ERROR )
	{
		string msg("UDPSocket::SetNonBlockingMode");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return NO_ERROR;
	}
}

int UDPSocket::SetSocketOpt(int level, int optname, const char *optval,
		int optlen)
{
	int result = setsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(const void*) optval, (socklen_t) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::SetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return result;
	}
}

int UDPSocket::GetSocketOpt(int level, int optname, char *optval, int *optlen)
{
	int result = getsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(void*) optval, (socklen_t*) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("UDPSocket::GetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return result;
	}
}

