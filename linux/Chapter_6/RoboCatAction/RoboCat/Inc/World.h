#ifndef World_h
#define World_h

/*
* the world tracks all the live game objects. Failry inefficient for now, but not that much of a problem
*/
class World
{

public:

	static void StaticInit();

	static unique_ptr< World >		sInstance;

	void AddGameObject( GameObjectPtr inGameObject );
	void RemoveGameObject( GameObjectPtr inGameObject );

	void Update();

	const vector< GameObjectPtr >&	GetGameObjects()	const	{ return mGameObjects; }

private:


	World();

	int	GetIndexOfGameObject( GameObjectPtr inGameObject );

	vector< GameObjectPtr >	mGameObjects;


};

#endif //World_h
