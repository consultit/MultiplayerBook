#ifndef InputAction_h
#define InputAction_h

enum EInputAction
{
	EIA_Pressed,
	EIA_Repeat,
	EIA_Released,
};

#endif //InputAction_h
