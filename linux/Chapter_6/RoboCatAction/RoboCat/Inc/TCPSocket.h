#ifndef TCPSocket_h
#define TCPSocket_h

class TCPSocket
{
public:
	~TCPSocket();
	int								Connect( const SocketAddress& inAddress );
	int								Bind( const SocketAddress& inToAddress );
	int								Listen( int inBackLog = 32 );
	shared_ptr< TCPSocket >			Accept( SocketAddress& inFromAddress );
	int32_t							Send( const void* inData, size_t inLen );
	int32_t							Receive( void* inBuffer, size_t inLen );
	int 							Shutdown(int how);
	int 							GetConnectedPeer(SocketAddress& address);
	SocketAddressPtr 				GetConnectedPeer();
	int 							SetSocketOpt(int level, int optname, const char *optval, int optlen);
	int 							GetSocketOpt(int level, int optname, char *optval, int *optlen);
private:
	friend class SocketUtil;
	TCPSocket( SOCKET inSocket ) : mSocket( inSocket ) {}
	SOCKET		mSocket;
};
typedef shared_ptr< TCPSocket > TCPSocketPtr;

#endif //TCPSocket_h
