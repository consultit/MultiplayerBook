#ifndef TextureManager_h
#define TextureManager_h

class TextureManager
{
public:
	static void StaticInit();

	static unique_ptr< TextureManager >		sInstance;

	TexturePtr	GetTexture( const string& inTextureName );

private:
	TextureManager();

	bool CacheTexture( string inName, const char* inFileName );

	unordered_map< string, TexturePtr >	mNameToTextureMap;
};

#endif //TextureManager_h
