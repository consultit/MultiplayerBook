#ifndef Engine_h
#define Engine_h

#include <SDL_events.h>

class Engine
{
public:
	static bool StaticInit();
	virtual ~Engine();
	static unique_ptr< Engine >	sInstance;

	virtual int		Run();
	void			SetShouldKeepRunning( bool inShouldKeepRunning ) { mShouldKeepRunning = inShouldKeepRunning; }
	virtual void	HandleEvent( SDL_Event* inEvent );
protected:

	Engine();

	virtual void	DoFrame();

private:


			
			int		DoRunLoop();

			bool	mShouldKeepRunning;



};

#endif //Engine_h
