#ifndef StringUtils_h
#define StringUtils_h

namespace StringUtils
{
	string GetCommandLineArg( int inIndex );

	string Sprintf( const char* inFormat, ... );

	void	Log( const char* inFormat, ... );
}

#define LOG( ... ) StringUtils::Log( __VA_ARGS__ );

#endif //StringUtils_h
