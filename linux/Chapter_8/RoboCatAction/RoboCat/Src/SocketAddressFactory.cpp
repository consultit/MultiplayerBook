#include "RoboCatPCH.h"


SocketAddressPtr SocketAddressFactory::CreateIPv4FromString( const string& inString )
{
	auto pos = inString.find_last_of( ':' );
	string host, service;
	if( pos != string::npos )
	{
		host = inString.substr( 0, pos );
		service = inString.substr( pos + 1 );
	}
	else
	{
		host = inString;
		//use default port...
		service = "0";
	}
	addrinfo hint;
	memset( &hint, 0, sizeof( hint ) );
	hint.ai_family = AF_INET;
	
	addrinfo* result;
	int error = getaddrinfo( host.c_str(), service.c_str(), &hint, &result );
#ifdef _WIN32
	if( error != 0 && result != nullptr )
#else
	if (error != 0)
#endif
	{
		string msg("SocketAddressFactory::CreateIPv4FromString");
#ifndef _WIN32
		msg += string(" -> ") + string(gai_strerror(error));
#endif
		SocketUtil::ReportError(msg.c_str());
		return nullptr;
	}
	
	while( !result->ai_addr && result->ai_next )
	{
		result = result->ai_next;
	}
	
	if( !result->ai_addr )
	{
		return nullptr;
	}
	
	auto toRet = make_shared< SocketAddress >( *result->ai_addr );
	
	freeaddrinfo( result );
	
	return toRet;
	
}
