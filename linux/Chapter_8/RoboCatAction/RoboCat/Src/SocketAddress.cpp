#include "RoboCatPCH.h"


string	SocketAddress::ToString() const
{
#if _WIN32
	const sockaddr_in* s = GetAsSockAddrIn();
	char destinationBuffer[ 128 ];
	InetNtop( s->sin_family, const_cast< in_addr* >( &s->sin_addr ), destinationBuffer, sizeof( destinationBuffer ) );
	return StringUtils::Sprintf( "%s:%d",
								destinationBuffer,
								ntohs( s->sin_port ) );
#elif __linux__
	const sockaddr_in* s = GetAsSockAddrIn();
	char destinationBuffer[INET_ADDRSTRLEN];
	if (!inet_ntop(AF_INET, &(s->sin_addr), destinationBuffer, INET_ADDRSTRLEN))
	{
		string msg("SocketAddress::ToString -> ");
		msg += string(strerror(errno));
		return StringUtils::Sprintf("%s", msg.c_str());
	}
	return StringUtils::Sprintf("%s:%d", destinationBuffer, ntohs(s->sin_port));
#else
	//not implement on mac for now...
	return string( "not implemented on mac for now" );
#endif
}

