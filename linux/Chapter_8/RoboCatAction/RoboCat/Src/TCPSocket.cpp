#include "RoboCatPCH.h"


int TCPSocket::Connect( const SocketAddress& inAddress )
{
	int result = connect( mSocket, &inAddress.mSockAddr, inAddress.GetSize() );
	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Connect");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	return NO_ERROR;
}

int TCPSocket::Listen( int inBackLog )
{
	int result = listen( mSocket, inBackLog );
	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Listen");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	return NO_ERROR;
}

TCPSocketPtr TCPSocket::Accept( SocketAddress& inFromAddress )
{
	socklen_t length = inFromAddress.GetSize();
	SOCKET newSocket = accept( mSocket, &inFromAddress.mSockAddr, &length );

	if( newSocket != INVALID_SOCKET )
	{
		return TCPSocketPtr( new TCPSocket( newSocket ) );
	}
	else
	{
		string msg("TCPSocket::Accept");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return nullptr;
	}
}

int32_t	TCPSocket::Send( const void* inData, size_t inLen )
{
	int bytesSentCount = send( mSocket,
#ifdef _WIN32
			static_cast<const char*>(inData),
#else
			inData,
#endif
			inLen, 0);
	if (bytesSentCount == SOCKET_ERROR)
	{
		string msg("TCPSocket::Send");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	return bytesSentCount;
}

int32_t	TCPSocket::Receive( void* inData, size_t inLen )
{
	int bytesReceivedCount = recv( mSocket,
#ifdef _WIN32
			static_cast<char*>(inData),
#else
			inData,
#endif
			inLen, 0);
	if (bytesReceivedCount == SOCKET_ERROR)
	{
		string msg("TCPSocket::Receive");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	return bytesReceivedCount;
}

int TCPSocket::Shutdown(int how = SocketUtil::BOTH)
{
	int result = shutdown(mSocket, how);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Shutdown");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}

	return NO_ERROR;
}

int TCPSocket::Bind( const SocketAddress& inBindAddress )
{
	int result = bind( mSocket, &inBindAddress.mSockAddr, inBindAddress.GetSize() );
	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::Bind");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}

	return NO_ERROR;
}

TCPSocket::~TCPSocket()
{
#if _WIN32
	closesocket( mSocket );
#else
	close( mSocket );
#endif
}

int TCPSocket::GetConnectedPeer(SocketAddress& address)
{
#ifdef _WIN32
	int addrlen;
#else
	socklen_t addrlen = sizeof(address.mSockAddr);
#endif
	int result = getpeername(mSocket, &address.mSockAddr, &addrlen);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::GetConnectedPeer");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return result;
	}
}

SocketAddressPtr TCPSocket::GetConnectedPeer()
{
	SocketAddressPtr address(new SocketAddress());
	int result = GetConnectedPeer(*address);
	if (result < 0)
	{
		return nullptr;
	}
	else
	{
		return address;
	}
}

int TCPSocket::SetSocketOpt(int level, int optname, const char *optval,
		int optlen)
{
	int result = setsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(const void*) optval, (socklen_t) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::SetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return result;
	}
}

int TCPSocket::GetSocketOpt(int level, int optname, char *optval, int *optlen)
{
	int result = getsockopt(mSocket, level, optname,
#ifdef _WIN32
			optval, optlen
#else
			(void*) optval, (socklen_t*) optlen
#endif
			);

	if (result == SOCKET_ERROR)
	{
		string msg("TCPSocket::GetSocketOpt");
#ifndef _WIN32
		msg += string(" -> ") + string(strerror(errno));
#endif
		//we'll return error as negative number to indicate less than requested amount of bytes sent...
		SocketUtil::ReportError(msg.c_str());
		return -SocketUtil::GetLastError();
	}
	else
	{
		return result;
	}
}

