#ifndef Move_h
#define Move_h

class Move
{
public:

	Move() {}

	Move( const InputState& inInputState, float inTimestamp, float inDeltaTime ) :
		mInputState( inInputState ),
		mTimestamp( inTimestamp ),
		mDeltaTime( inDeltaTime )
	{}


	const InputState&	GetInputState()	const		{ return mInputState; }
	float				GetTimestamp()	const		{ return mTimestamp; }
	float				GetDeltaTime()	const		{ return mDeltaTime; }

	bool Write( OutputMemoryBitStream& inOutputStream ) const;
	bool Read( InputMemoryBitStream& inInputStream );

private:
	InputState	mInputState;
	float		mTimestamp;
	float		mDeltaTime;

};



#endif //Move_h
