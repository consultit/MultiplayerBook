#ifndef SocketUtil_h
#define SocketUtil_h

enum SocketAddressFamily
{
	INET = AF_INET,
	INET6 = AF_INET6
};

class SocketUtil
{
public:
	enum ShutdownHow
	{
#if _WIN32
		SEND = SD_SEND,
		RECEIVE = SD_RECEIVE,
		BOTH = SD_BOTH
#else
		SEND = SHUT_WR, RECEIVE = SHUT_RD, BOTH = SHUT_RDWR
#endif
	};
	
	class SocketLib
	{
	public:
		SocketLib() :
				_setup
				{ true }
		{
			//init (never executed for posix)
			if (!SocketUtil::StaticInit())
			{
				string msg("SocketUtil::StaticInit -> ");
#ifndef _WIN32
				msg += string(strerror(errno));
#endif //_WIN32
				SocketUtil::ReportError(msg.c_str());
				_setup = false;
			}
		}
		~SocketLib()
		{
			if (_setup)
			{
				//cleanup
				SocketUtil::CleanUp();
			}
		}
		bool isSetup()
		{
			return _setup;
		}
		;
	private:
		bool _setup;
	};

	static bool			StaticInit();
	static void			CleanUp();

	static void			ReportError( const char* inOperationDesc );
	static int			GetLastError();

	static int			Select( const vector< TCPSocketPtr >* inReadSet,
								vector< TCPSocketPtr >* outReadSet,
								const vector< TCPSocketPtr >* inWriteSet,
								vector< TCPSocketPtr >* outWriteSet,
								const vector< TCPSocketPtr >* inExceptSet,
								vector< TCPSocketPtr >* outExceptSet );

	static UDPSocketPtr	CreateUDPSocket( SocketAddressFamily inFamily );
	static TCPSocketPtr	CreateTCPSocket( SocketAddressFamily inFamily );

private:

	inline static fd_set* FillSetFromVector( fd_set& outSet, const vector< TCPSocketPtr >* inSockets, int& ioNaxNfds );
	inline static void FillVectorFromSet( vector< TCPSocketPtr >* outSockets, const vector< TCPSocketPtr >* inSockets, const fd_set& inSet );
};

#endif //SocketUtil_h
