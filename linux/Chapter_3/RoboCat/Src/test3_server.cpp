#include "RoboCatPCH.h"
#include <iostream>
#include <algorithm>

using namespace std;

static string listenAddress = "0.0.0.0:8888";
//UDP server
static const int UDP_SEGMENT_SIZE = 1300;
static int startUDPServer(const string& address);
static void ProcessDataFromUDPClient(const SocketAddress& address,
		char* segment, int dataReceived);
//TCP server
static const int TCP_SEGMENT_SIZE = 16384;
static int startTCPServer(const string& address);
static void ProcessNewTCPClient(const SocketAddress& address);
static void ProcessDataFromTCPClient(TCPSocketPtr TCPSocket, char* segment,
		int dataReceived);

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;
#endif

	//command line arguments: test3_server [udp|tcp [listen.address:port]]
	bool tcpServer = false;
	int numArgs = min(3, argc);
	switch (numArgs)
	{
	case 3:	//listen.address:port
		listenAddress = argv[2];
	case 2:	//udp|tcp
		tcpServer = strcmp(argv[1], "tcp") == 0 ? true : false;
	case 1:	//default arguments
	default:
		break;
	}
	cout << "Creating " << (tcpServer ? "TCP" : "UDP")
			<< " server listening on " << listenAddress << endl;

	///TESTS
	SocketUtil::SocketLib library;
	if (!library.isSetup())
	{
		return (-1);
	}
	//
	if (!tcpServer) ///UDP server
	{
		return startUDPServer(listenAddress);
	}
	else ///TCP server
	{
		return startTCPServer(listenAddress);
	}
}

int startUDPServer(const string& address)
{
	UDPSocketPtr serverSock = SocketUtil::CreateUDPSocket(INET);
	if (!serverSock)
	{
		return (-1);
	}
	PRINT_DEBUG("UDP server: socket created");

	//get address
	SocketAddressPtr inBindAddress = SocketAddressFactory::CreateIPv4FromString(
			address);
	if (!inBindAddress)
	{
		return (-1);
	}
	PRINT_DEBUG("UDP server: address ok");
	//bind address
	int result = serverSock->Bind(*inBindAddress);
	if (result < 0)
	{
		return (result);
	}
	PRINT_DEBUG("UDP server: bound and listening on " << address);

	//receive datagrams
	char inToReceive[UDP_SEGMENT_SIZE];
	SocketAddress outFromAddress;
	// start the loop
	while (true)
	{
		int numBytes = serverSock->ReceiveFrom((void*) inToReceive,
				UDP_SEGMENT_SIZE, outFromAddress);
		if (numBytes < 0)
		{
			return (numBytes);
		}
		// process data from client
		ProcessDataFromUDPClient(outFromAddress, inToReceive, numBytes);
	}
	return 0;
}

void ProcessDataFromUDPClient(const SocketAddress& address, char* segment,
		int dataReceived)
{
	//force null terminated string
	segment[max(0, dataReceived - 1)] = '\0';
	cout << address.ToString() << " -> " << string(segment) << endl;
}

int startTCPServer(const string& address)
{
	TCPSocketPtr serverSock = SocketUtil::CreateTCPSocket(INET);
	if (!serverSock)
	{
		return (-1);
	}
	PRINT_DEBUG("TCP server: socket created");

	//get address
	SocketAddressPtr inBindAddress = SocketAddressFactory::CreateIPv4FromString(
			address);
	if (!inBindAddress)
	{
		return (-1);
	}
	PRINT_DEBUG("TCP server: address ok");
	//bind address
	int result = serverSock->Bind(*inBindAddress);
	if (result < 0)
	{
		return (result);
	}
	PRINT_DEBUG("TCP server: bound and listening on " << address);

	//listen for connection
	result = serverSock->Listen(SOMAXCONN);
	if (result < 0)
	{
		return (result);
	}

	vector<TCPSocketPtr> readBlockSockets;
	readBlockSockets.push_back(serverSock);

	vector<TCPSocketPtr> readableSockets;

	// start the loop
	while (true)
	{
		PRINT_DEBUG("TCP server: monitoring sockets...");
		if (SocketUtil::Select(&readBlockSockets, &readableSockets, nullptr,
				nullptr, nullptr, nullptr) >= 0)
		{
			//we got a packet—loop through the set ones...
			for (const TCPSocketPtr& socket : readableSockets)
			{
				if (socket == serverSock)
				{
					//it's the listen socket, accept a new connection
					SocketAddress newClientAddress;
					auto newSocket = serverSock->Accept(newClientAddress);
					if (!newSocket)
					{
						break;
					}
					readBlockSockets.push_back(newSocket);
					ProcessNewTCPClient(newClientAddress);
				}
				else
				{
					//it's a regular socket—process the data...
					char segment[TCP_SEGMENT_SIZE];
					int dataReceived = socket->Receive(segment,
							TCP_SEGMENT_SIZE);
					if (dataReceived > 0)
					{
						ProcessDataFromTCPClient(socket, segment, dataReceived);
					}
					else if (dataReceived == 0)
					{
						//peer send FIN: shutdown
						socket->Shutdown(SocketUtil::RECEIVE);
						SocketAddressPtr address = socket->GetConnectedPeer();
						PRINT_DEBUG("TCP server: closing connection with "
								<< (address ? address->ToString() : "UNKNOWN")
								<< " client");
						//remove socket from readBlockSockets
						auto iter = find(readBlockSockets.begin(),
								readBlockSockets.end(), socket);
						readBlockSockets.erase(iter);
					}
					else //(dataReceived < 0)
					{
						break;
					}
				}
			}
		}
		else
		{
			return -1;
		}
	}
	return 0;
}

void ProcessNewTCPClient(const SocketAddress& address)
{
	PRINT_DEBUG("TCP server: " << address.ToString() << " -> " << string("connected"));
}

void ProcessDataFromTCPClient(TCPSocketPtr socket, char* segment,
		int dataReceived)
{
	SocketAddressPtr address = socket->GetConnectedPeer();
	//force null terminated string
	segment[max(0, dataReceived - 1)] = '\0';
	cout << (address ? address->ToString() : "UNKNOWN") << " -> " << string(segment) << endl;
}
