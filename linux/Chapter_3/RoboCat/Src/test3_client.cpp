#include "RoboCatPCH.h"
#include <iostream>

using namespace std;

static string clientBindAddress = "";
static string serverAddress = "debian:8888";
//UDP client
static const int UDP_SEGMENT_SIZE = 1300;
static int startUDPClient(const string& address);
static int ProcessDataToUDPServer(const SocketAddress& address,
		UDPSocketPtr socket, const string& msg);
//TCP client
static const int TCP_SEGMENT_SIZE = 16384;
static int startTCPClient(const string& address);
static int ProcessDataToTCPServer(TCPSocketPtr socket, const string& msg);

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;
#endif

	//command line arguments: test3_client [udp|tcp [server.address:port [bind.address:port]]]
	bool tcpClient = false;
	int numArgs = min(4, argc);
	switch (numArgs)
	{
	case 4:	//bind.address:port
		clientBindAddress = argv[3];
	case 3:	//server.address:port
		serverAddress = argv[2];
	case 2:	//udp|tcp
		tcpClient = strcmp(argv[1], "tcp") == 0 ? true : false;
	case 1:	//default arguments
	default:
		break;
	}
	cout << "Creating " << (tcpClient ? "TCP" : "UDP")
			<< " client transmitting to " << serverAddress << " server"
			<< (!clientBindAddress.empty() ?
					string(" and bound to ") + clientBindAddress : "") << endl;

	///TESTS
	SocketUtil::SocketLib library;
	if (!library.isSetup())
	{
		return (-1);
	}
	//
	if (!tcpClient) ///UDP Client
	{
		return startUDPClient(serverAddress);
	}
	else ///TCP client
	{
		return startTCPClient(serverAddress);
	}
}

int startUDPClient(const string& address)
{
	UDPSocketPtr clientSock = SocketUtil::CreateUDPSocket(INET);
	if (!clientSock)
	{
		return (-1);
	}
	PRINT_DEBUG("UDP client: socket created");

	//bind is optional
	if (!clientBindAddress.empty())
	{
		SocketAddressPtr inBindAddress =
				SocketAddressFactory::CreateIPv4FromString(clientBindAddress);
		if (!inBindAddress)
		{
			return (-1);
		}
		PRINT_DEBUG("UDP client: bind address ok");
		int result = clientSock->Bind(*inBindAddress);
		if (result != NO_ERROR)
		{
			return (result);
		}
		PRINT_DEBUG("UDP client: bound and ready on " << clientBindAddress);
	}

	//send data to server
	SocketAddressPtr inToAddress =
			SocketAddressFactory::CreateIPv4FromString(address);
	if (!inToAddress)
	{
		return (-1);
	}
	PRINT_DEBUG("UDP client: server address ok -> " << address);
	//
	while (true)
	{
		string msg;
		if (getline(cin, msg).eof())
		{
			return (-1);
		}
		if (ProcessDataToUDPServer(*inToAddress, clientSock, msg) < 0)
		{
			return (-1);
		}
	}
	//
	return (0);
}

int ProcessDataToUDPServer(const SocketAddress& address,
		UDPSocketPtr socket, const string& msg)
{
	return socket->SendTo((const void*) msg.c_str(), UDP_SEGMENT_SIZE, address);
}

int startTCPClient(const string& address)
{
	TCPSocketPtr clientSock = SocketUtil::CreateTCPSocket(INET);
	if (!clientSock)
	{
		return (-1);
	}
	PRINT_DEBUG("TCP client: socket created");

	//bind is optional
	if (!clientBindAddress.empty())
	{
		SocketAddressPtr inBindAddress =
				SocketAddressFactory::CreateIPv4FromString(clientBindAddress);
		if (!inBindAddress)
		{
			return (-1);
		}
		PRINT_DEBUG("TCP client: bind address ok");
		int result = clientSock->Bind(*inBindAddress);
		if (result != NO_ERROR)
		{
			return (result);
		}
		PRINT_DEBUG("TCP client: bound and ready on " << clientBindAddress);
	}
	//connect to server
	SocketAddressPtr inToAddress =
			SocketAddressFactory::CreateIPv4FromString(address);
	if (!inToAddress)
	{
		return (-1);
	}
	PRINT_DEBUG("TCP client: server address ok -> " << address);
	if (clientSock->Connect(*inToAddress) < 0)
	{
		return (-1);
	}
	PRINT_DEBUG("TCP client: connected to server");

	//send data to server
	while (true)
	{
		string msg;
		if (getline(cin, msg).eof())
		{
			PRINT_DEBUG("TCP client: shutting down");
			clientSock->Shutdown(SocketUtil::SEND);
			return(-1);
		}
		if (ProcessDataToTCPServer(clientSock, msg) < 0)
		{
			return(-1);
		}
	}
	//
	return (0);
}

int ProcessDataToTCPServer(TCPSocketPtr socket, const string& msg)
{
	size_t len = msg.size() + 1;
	return socket->Send((const void*) msg.c_str(), min(len, (size_t)TCP_SEGMENT_SIZE));
}
