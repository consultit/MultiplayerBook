#include "RoboCatPCH.h"
#include <iostream>

using namespace std;

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

	///TESTS
	//init (never executed for posix)
	if (!SocketUtil::StaticInit())
	{
		string msg("SocketUtil::StaticInit -> ");
#ifndef _WIN32
		msg += string(strerror(errno));
#endif //_WIN32
		SocketUtil::ReportError(msg.c_str());
		exit(SocketUtil::GetLastError());
	}
	//Socket Address
	string addrStr("10.0.24.23:80");
	SocketAddressPtr addrPtr = SocketAddressFactory::CreateIPv4FromString(
			addrStr);
	if (addrPtr)
	{
		cout << addrPtr->ToString() << endl;
	}
	//
	SocketAddress addr(0x0a001817, 80);
	cout << addr.ToString() << endl;
	//
	SocketAddressPtr addrPtr1 = SocketAddressFactory::CreateIPv4FromString(
			"www.repubblica.it:443");
	cout << addrPtr1->ToString() << endl;
	SocketAddress addr1(*addrPtr1);
	cout << addr1.ToString() << endl;

	//cleanup
	SocketUtil::CleanUp();
}
#endif

