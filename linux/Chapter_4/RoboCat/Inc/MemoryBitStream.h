#ifndef MemoryBitStream_h
#define MemoryBitStream_h

#include <cstdint>
#include <cstdlib>
#include <string>
#include <cstring>
#include <bitset>

class GameObject;

inline uint32_t ConvertToFixed( float inNumber, float inMin, float inPrecision )
{
	return static_cast< int > ( ( inNumber - inMin ) / inPrecision );
}

inline float ConvertFromFixed( uint32_t inNumber, float inMin, float inPrecision )
{
	return static_cast< float >( inNumber ) * inPrecision + inMin;
}


class OutputMemoryBitStream
{
public:

	OutputMemoryBitStream() :
		mBitHead(0),
		mBuffer(nullptr)
	{
		ReallocBuffer( 1500 * 8 );
	}

	~OutputMemoryBitStream()	{ free( mBuffer ); }

	void		WriteBits( uint8_t inData, uint32_t inBitCount );
	void		WriteBits( const void* inData, uint32_t inBitCount );

	const 	char*	GetBufferPtr()		const	{ return mBuffer; }
	uint32_t		GetBitLength()		const	{ return mBitHead; }
	uint32_t		GetByteLength()		const	{ return ( mBitHead + 7 ) >> 3; }

	void WriteBytes( const void* inData, uint32_t inByteCount )	{ WriteBits( inData, inByteCount << 3 ); }

	/*
	void Write( uint32_t inData, uint32_t inBitCount = 32 )	{ WriteBits( &inData, inBitCount ); }
	void Write( int inData, uint32_t inBitCount = 32 )		{ WriteBits( &inData, inBitCount ); }
	void Write( float inData )								{ WriteBits( &inData, 32 ); }

	void Write( uint16_t inData, uint32_t inBitCount = 16 )	{ WriteBits( &inData, inBitCount ); }
	void Write( int16_t inData, uint32_t inBitCount = 16 )	{ WriteBits( &inData, inBitCount ); }

	void Write( uint8_t inData, uint32_t inBitCount = 8 )	{ WriteBits( &inData, inBitCount ); }
	*/
	
	template< typename T >
	void Write( T inData, uint32_t inBitCount = sizeof( T ) * 8 )
	{
		static_assert( is_arithmetic< T >::value ||
					  is_enum< T >::value,
					  "Generic Write only supports primitive data types" );
		if ( STREAM_ENDIANNESS == PLATFORM_ENDIANNESS)
		{
			WriteBits((const void*) &inData, inBitCount);
		}
		else
		{
			T swappedData = ByteSwap(inData);
			WriteBits((const void*) &swappedData, inBitCount);
		}
	}
	
	void 		Write( bool inData )								{ WriteBits( &inData, 1 ); }
	
	void		Write( const Vector3& inVector );	
	void		Write( const Quaternion& inQuat );

	void Write( const string& inString )
	{
		uint32_t elementCount = static_cast< uint32_t >( inString.size() );
		Write( elementCount );
		for( const auto& element : inString )
		{
			Write( element );
		}
	}
	
private:
	void		ReallocBuffer( uint32_t inNewBitCapacity );

	char*		mBuffer;
	uint32_t	mBitHead;
	uint32_t	mBitCapacity;
};

class InputMemoryBitStream
{
public:
	
	InputMemoryBitStream( char* inBuffer, uint32_t inBitCount ) :
	mBuffer( inBuffer ),
	mBitCapacity( inBitCount ),
	mBitHead( 0 ),
	mIsBufferOwner( false ) {}
	
	InputMemoryBitStream( const InputMemoryBitStream& inOther ) :
	mBitCapacity( inOther.mBitCapacity ),
	mBitHead( inOther.mBitHead ),
	mIsBufferOwner( true )
	{
		//allocate buffer of right size
		int byteCount = ( mBitCapacity + 7 ) / 8;
		mBuffer = static_cast< char* >( malloc( byteCount ) );
		//copy
		memcpy( mBuffer, inOther.mBuffer, byteCount );
	}
	
	~InputMemoryBitStream()	{ if( mIsBufferOwner ) { free( mBuffer ); }; }
	
	const 	char*	GetBufferPtr()		const	{ return mBuffer; }
	uint32_t	GetRemainingBitCount() 	const { return mBitCapacity - mBitHead; }

	void		ReadBits( uint8_t& outData, uint32_t inBitCount );
	void		ReadBits( void* outData, uint32_t inBitCount );

	void		ReadBytes( void* outData, uint32_t inByteCount )		{ ReadBits( outData, inByteCount << 3 ); }

	template< typename T >
	void Read( T& inData, uint32_t inBitCount = sizeof( T ) * 8 )
	{
		static_assert( is_arithmetic< T >::value ||
					  is_enum< T >::value,
					  "Generic Read only supports primitive data types" );
		ReadBits((void*) &inData, inBitCount);
		if ( STREAM_ENDIANNESS != PLATFORM_ENDIANNESS)
		{
			T swappedData = ByteSwap(inData);
			memcpy((void*) &inData, (const void*) &swappedData, sizeof(inData));
		}
	}
	
/*	
	void		Read( uint32_t& outData, uint32_t inBitCount = 32 )		{ ReadBits( &outData, inBitCount ); }
	void		Read( int& outData, uint32_t inBitCount = 32 )			{ ReadBits( &outData, inBitCount ); }
	void		Read( float& outData )									{ ReadBits( &outData, 32 ); }

	void		Read( uint16_t& outData, uint32_t inBitCount = 16 )		{ ReadBits( &outData, inBitCount ); }
	void		Read( int16_t& outData, uint32_t inBitCount = 16 )		{ ReadBits( &outData, inBitCount ); }

	void		Read( uint8_t& outData, uint32_t inBitCount = 8 )		{ ReadBits( &outData, inBitCount ); }
*/
	void		Read( bool& outData )									{ ReadBits( &outData, 1 ); }

	void		Read( Quaternion& outQuat );

	void		ResetToCapacity( uint32_t inByteCapacity )				{ mBitCapacity = inByteCapacity << 3; mBitHead = 0; }


	void Read( string& inString )
	{
		uint32_t elementCount;
		Read( elementCount );
		inString.resize( elementCount );
		for( auto& element : inString )
		{
			Read( element );
		}
	}

	void Read( Vector3& inVector );

private:
	char*		mBuffer;
	uint32_t	mBitHead;
	uint32_t	mBitCapacity;
	bool		mIsBufferOwner;

};

inline void testOutputMemoryBitStream()
{
	cout << "testOutputMemoryBitStream" << endl;
	int8_t i8 = 0b110101; //53 = Age
	int16_t i16 = 0b10111010100; //1492 = Year of Discovery of America
	int32_t i32 = 0b11100111001000010110000101; //60589445 = Italian population at 2017-01-01

	// print values
	cout << (int) i8 << ": " << bitset<8>(i8) << endl;
	cout << i16 << ": " << bitset<16>(i16) << endl;
	cout << i32 << ": " << bitset<32>(i32) << endl;

	// write values
	OutputMemoryBitStream outStream;
	outStream.Write(i8, 6);
	outStream.Write(i16, 11);
	outStream.Write(i32, 26);
	// save to file
	ofstream fout("dataBit.ser", ios::out | ios::binary);
	fout.write(outStream.GetBufferPtr(), outStream.GetByteLength());
}

inline void testInputMemoryBitStream()
{
	cout << "testInputMemoryBitStream" << endl;
	// load from file
	ifstream fin("dataBit.ser", ios::in | ios::binary | ios::ate);
	uint32_t fileSize = fin.tellg();
	fin.seekg(ios::beg);
	char* inBuffer = new char[fileSize];
	fin.read(inBuffer, fileSize);

	// read values
	InputMemoryBitStream inStream(inBuffer, fileSize * 8);
	int8_t i8;	//53 = Age
	int16_t i16;	//1492 = Year of Discovery of America
	int32_t i32;	//60589445 = Italian population at 2017-01-01
	inStream.Read(i8, 6);
	inStream.Read(i16, 11);
	inStream.Read(i32, 26);
	// print values
	cout << (int) i8 << ": " << bitset<8>(i8) << endl;
	cout << i16 << ": " << bitset<16>(i16) << endl;
	cout << i32 << ": " << bitset<32>(i32) << endl;
}

#endif //MemoryBitStream_h
