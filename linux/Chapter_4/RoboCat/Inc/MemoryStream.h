#ifndef MemoryStream_h
#define MemoryStream_h

#include <cstdlib>
#include <cstdint>
#include <type_traits>
#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

class GameObject;
class LinkingContext;

class OutputMemoryStream
{
public:
	OutputMemoryStream() :
			mBuffer(nullptr), mHead(0), mCapacity(0), mLinkingContext(nullptr)
	{ ReallocBuffer( 32 ); }
	
	~OutputMemoryStream()	{ free( mBuffer ); }
	
	//get a pointer to the data in the stream
	const 	char*		GetBufferPtr()	const	{ return mBuffer; }
			uint32_t	GetLength()		const	{ return mHead; }
	
			void		Write( const void* inData,
								uint32_t inByteCount );
	
	template< typename T > void Write( T inData )
	{
		static_assert( is_arithmetic< T >::value ||
					  is_enum< T >::value,
					  "Generic Write only supports primitive data types" );
		
		if( STREAM_ENDIANNESS == PLATFORM_ENDIANNESS )
		{
			Write((const void*) &inData, sizeof(inData));
		}
		else
		{
			T swappedData = ByteSwap( inData );
			Write((const void*) &swappedData, sizeof( swappedData ) );
		}
		
	}
	
	template< typename T >
	void Write( const vector< T >& inVector )
	{
		uint32_t elementCount = inVector.size();
		Write( elementCount );
		for( const T& element : inVector )
		{
			Write( element );
		}
	}
	
	void Write( const string& inString )
	{
		uint32_t elementCount = inString.size() ;
		Write( elementCount );
		Write( inString.data(), elementCount * sizeof( char ) );
	}
	
	void Write( const GameObject* inGameObject )
	{
		uint32_t networkId = mLinkingContext->GetNetworkId( const_cast< GameObject* >( inGameObject ), false );
		Write( networkId );
	}
	
	
private:
			void		ReallocBuffer( uint32_t inNewLength );
	
	char*		mBuffer;
	uint32_t	mHead;
	uint32_t	mCapacity;
	
	LinkingContext* mLinkingContext;
};

class InputMemoryStream
{
public:
	InputMemoryStream( char* inBuffer, uint32_t inByteCount ) :
	mBuffer( inBuffer ), mCapacity( inByteCount ), mHead( 0 ),
	mLinkingContext( nullptr ) {}

	~InputMemoryStream()	{ free( mBuffer ); }
		
	uint32_t		GetRemainingDataSize() const
					{ return mCapacity - mHead; }
	
	void		Read( void* outData, uint32_t inByteCount );


	template< typename T > void Read( T& outData )
	{
		static_assert( is_arithmetic< T >::value ||
					   is_enum< T >::value,
					   "Generic Read only supports primitive data types" );
		Read((void*) &outData, sizeof(outData));
		if ( STREAM_ENDIANNESS != PLATFORM_ENDIANNESS)
		{
			T swappedData = ByteSwap(outData);
			memcpy((void*) &outData, (const void*) &swappedData,
					sizeof(outData));
		}
	}
	
	template< typename T >
	void Read( vector< T >& outVector )
	{
		uint32_t elementCount;
		Read( elementCount );
		outVector.resize( elementCount );
		for( const T& element : outVector )
		{
			Read( element );
		}
	}

	void Read(string& inString)
	{
		uint32_t elementCount;
		Read(elementCount);
		char* buf = new char[elementCount * sizeof(char)];
		Read(buf, elementCount * sizeof(char));
		inString = buf;
		delete[] buf;
	}
	
	void Read( GameObject*& outGameObject )
	{
		uint32_t networkId;
		Read( networkId );
		outGameObject = mLinkingContext->GetGameObject( networkId );
	}
	
private:
	char*		mBuffer;
	uint32_t	mHead;
	uint32_t	mCapacity;

	LinkingContext* mLinkingContext;
};

inline void testEndianness()
{
	cout << "testEndianness" << endl;
	if (IS_LITTLE_ENDIAN)
	{
		cout << "little endianness" << endl;
	}
	else if (IS_BIG_ENDIAN)
	{
		cout << "big endianness" << endl;
	}
	else
	{
		cout << "unknown endianness" << endl;
	}
}

inline void testOutputMemoryStream()
{
	cout << "testOutputMemoryStream" << endl;
	int32_t test = 0x12345678;
	float floatTest = 1.f;
	double doubleTest = 1.234567e+89;
	vector<int32_t> vintTest
	{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
	// print values
	cout << test << endl;
	cout << floatTest << endl;
	cout << doubleTest << endl;
	for (auto i : vintTest)
	{
		cout << i << ", ";
	}
	cout << endl;

	// write values
	OutputMemoryStream outStream;
	outStream.Write(test);
	outStream.Write(floatTest);
	outStream.Write(doubleTest);
	outStream.Write(vintTest);
	// save to file
	ofstream fout("data.ser", ios::out | ios::binary);
	fout.write(outStream.GetBufferPtr(), outStream.GetLength());
}

inline void testInputMemoryStream()
{
	cout << "testInputMemoryStream" << endl;
	// load from file
	ifstream fin("data.ser", ios::in | ios::binary | ios::ate);
	uint32_t fileSize = fin.tellg();
	fin.seekg(ios::beg);
	char* inBuffer = new char[fileSize];
	fin.read(inBuffer, fileSize);

	// read values
	InputMemoryStream inStream(inBuffer, fileSize);
	int32_t test;
	float floatTest;
	double doubleTest;
	vector<int32_t> vintTest;
	inStream.Read(test);
	inStream.Read(floatTest);
	inStream.Read(doubleTest);
	inStream.Read(vintTest);
	// print values
	cout << test << endl;
	cout << floatTest << endl;
	cout << doubleTest << endl;
	for (auto i : vintTest)
	{
		cout << i << ", ";
	}
	cout << endl;
}

#endif //MemoryStream_h
