#include <RoboCatPCH.h>

void OutputMemoryStream::ReallocBuffer( uint32_t inNewLength )
{
	mBuffer = static_cast< char* >( realloc( mBuffer, inNewLength ) );
	if (!mBuffer)
	{
		cerr
				<< "OutputMemoryStream::ReallocBuffer(): failed to allocate storage."
				<< endl;
		exit(-1);
	}
	mCapacity = inNewLength;
}

void OutputMemoryStream::Write( const void* inData,
								uint32_t inByteCount )
{
	//make sure we have space...
	uint32_t resultHead = mHead + inByteCount;
	if( resultHead > mCapacity )
	{
		ReallocBuffer( max( mCapacity * 2, resultHead ) );
	}
	
	//copy into buffer at head
	memcpy( mBuffer + mHead, inData, inByteCount );
	
	//increment head for next write
	mHead = resultHead;
}


void InputMemoryStream::Read( void* outData,
							  uint32_t inByteCount )
{
	uint32_t resultHead = mHead + inByteCount;
	if( resultHead > mCapacity )
	{
		if (mHead < mCapacity)
		{
			cerr << "InputMemoryStream::Read(): cannot read "
					<< resultHead - mCapacity << " bytes." << endl;
			inByteCount = mCapacity - mHead;
			resultHead = mCapacity;
		}
		else
		{
			cerr << "InputMemoryStream::Read(): cannot read " << inByteCount
					<< " bytes." << endl;
			return;
		}
	}
	
	memcpy( outData, mBuffer + mHead, inByteCount );
	
	mHead = resultHead;
}
